/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
export default {
  UUID: {
    description: "This is the application UUID.",
    defaultValue: "weever",
    defaultValueInProduction: true,
  },
  SCHEMA_NAMESPACE_MAPPING: {
    description: "The ReSource ontology schema namespace mapping",
    defaultValue: JSON.stringify({
      rdfs: "http://www.w3.org/2000/01/rdf-schema#",
      skos: "http://www.w3.org/2004/02/skos/core#",
      geonames: "https://www.geonames.org/",
      foaf: "http://xmlns.com/foaf/0.1/",
      prov: "http://www.w3.org/ns/prov#",
      sioc: "http://rdfs.org/sioc/ns#",
      dc: "http://purl.org/dc/terms/",
      mnx: "http://ns.mnemotix.com/ontologies/2019/8/generic-model/",
    }),
    defaultValueInProduction: true,
  },
  NODES_NAMESPACE_URI: {
    description:
      "The nodes (or individuals) (or class instances) namespace URI",
    defaultValue: "https://data.mnemotix.com/coopair/",
    defaultValueInProduction: true,
  },
  NODES_PREFIX: {
    description: "The nodes or individuals) (or class instances) namespace URI",
    defaultValue: "mnxd",
    defaultValueInProduction: true,
  },
  NODES_NAMED_GRAPH: {
    description: "The nodes named graph URI",
    defaultValue: "https://data.mnemotix.com/coopair/NG",
    defaultValueInProduction: true,
  },
  INDEX_PREFIX_TYPES_WITH: {
    description: "Prefix all index types with a prefix",
    defaultValue: "local-coopair-",
  },
  INDEX_VERSION: {
    defaultValue: "NATIVE",
    defaultValueInProduction: true,
  },
  INDEX_DISABLED: {
    description: "Is index disabled",
    defaultValue: "0",
    defaultValueInProduction: true,
  },
  RABBITMQ_EXCHANGE_NAME: {
    description: "This is RabbitMQ exchange name.",
    defaultValue: "local-coopair",
  },
  PUBLIC_EXPLORER_ENABLED: {
    description: "Enable user to access public data without to be logged in",
    defaultValue: "1",
    defaultValueInProduction: true,
    exposeInGraphQL: true,
  },
  CONCEPT_SUBMISSION_VOCABULARY_ID: {
    defaultValue: () => `vocabulary/9lggwygfvk220b`,
    defaultValueInProduction: true,
  },
};
